def measure(repeat=1, &block)
  total = 0.0
  repeat.times do
    start = Time.now
    block.call
    total += Time.now - start
  end
  total / repeat
end
