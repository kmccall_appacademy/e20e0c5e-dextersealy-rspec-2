def reverser
  string = yield
  string.split().map { |word| word.reverse }.join(" ")
end

def adder(increment=1)
  yield + increment
end

def repeater(count=1, &block)
  count.times {block.call}
end
